package utpfactorydesignpattern;

public interface Shape {
    void draw();
}
